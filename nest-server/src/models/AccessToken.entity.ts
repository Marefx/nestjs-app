import { Entity, Column, PrimaryGeneratedColumn,  ManyToOne } from 'typeorm';
import UserEntity from "./User.entity";

@Entity({name: "access_tokens"})
export class AccessTokenEntity {
    @PrimaryGeneratedColumn()
    id: number;
    // https://wanago.io/2020/06/22/api-nestjs-relationships-postgres-typeorm/
    @ManyToOne(() => UserEntity, (user: UserEntity) => user.accessTokens)
    user: UserEntity;

    @Column()
    token: string;

    @Column()
    lats_request_ip: string;

    @Column({type: "text"})
    lats_request_url: string;

    @Column()
    last_used_at: Date;

    @Column()
    updated_at: Date;

    @Column()
    created_at: Date;
}

export default AccessTokenEntity;
