## Installation before proceeding

```bash
$ npm install @nestjs/cli @angular/cli -g
```

## Structure

- nest-server (Backend NestJS 7)
- ng-client (Frontend Angular 10)

## Nginx

nginx.conf

```bash
upstream nestjs_websocket {
        ip_hash;
        server nestjs:9898;
}

map $http_upgrade $connection_upgrade {
        default upgrade;
        '' close;
}

server {
        listen 8020;
        server_name _;
        include /etc/nginx/templates/ssl.tmpl;

        ssl_certificate /etc/letsencrypt/live/test.local.dev/fullchain.pem; # managed by Certbot
        ssl_certificate_key /etc/letsencrypt/live/test.local.dev/privkey.pem; # managed by Certbot

        location / {
                proxy_set_header X-Real-IP $remote_addr;
                proxy_set_header X-Real-Port $remote_port;
                proxy_pass http://nestjs_websocket;
                proxy_http_version 1.1;
                proxy_set_header Upgrade $http_upgrade;
                proxy_set_header Connection $connection_upgrade;
      }
}

upstream nextjs_upstream {
        server nestjs:4545;
        # Could add additional servers here for load-balancing
}

server {
        listen 443 ssl http2;
        listen [::]:443 ssl http2;
        include /etc/nginx/templates/ssl.tmpl;

        server_tokens off;

        gzip on;
        gzip_proxied any;
        gzip_comp_level 4;
        gzip_types text/css application/javascript image/svg+xml;

        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection 'upgrade';
        proxy_set_header Host $host;
        proxy_cache_bypass $http_upgrade;

        root /var/www/html/nestjs-ng-news/ng-client/dist/ng-client;
        index index.html;
        server_name _;

        access_log /var/log/nginx/nestjs.access.log;
        error_log /var/log/nginx/nestjs.error.log;

        ssl_certificate /etc/letsencrypt/live/test.local.dev/fullchain.pem; # managed by Certbot
        ssl_certificate_key /etc/letsencrypt/live/test.local.dev/privkey.pem; # managed by Certbot

        location / {
                try_files $uri $uri/ /index.html;
        }
        location /_next/static {
#               proxy_cache STATIC;
                proxy_pass https://nextjs_upstream;
                add_header X-Cache-Status $upstream_cache_status;
        }
        location /static {
#               proxy_cache STATIC;
                proxy_ignore_headers Cache-Control;
                proxy_cache_valid 60m;
                proxy_pass https://nextjs_upstream;
                add_header X-Cache-Status $upstream_cache_status;
        }
        location /api/v1 {
                proxy_pass https://nextjs_upstream;
        }
        location ~ /\.ht {
                deny all;
        }
        location = /favicon.ico {
                log_not_found off;
                access_log off;
        }
        location = /robots.txt {
                allow all;
                log_not_found off;
                access_log off;
        }
        location ~* \.(js|css|png|jpg|jpeg|gif|ico)$ {
                expires max;
                log_not_found off;
        }
}
```

## PM2

Start backend process in background to serve API

#### Installation

 ```bash
 $ npm install pm2@latest -g
 ```

#### Start process

 ```bash
 $ pm2 start /path/to/dist/main.js --name="NestJS News"
 ```
##### Optional

```bash
{
        "apps" : [{
                "env": {
                        "APP_ENV": "production",
                        "APP_DEBUG": "false",
                        "EXPRESS_PORT":4545,
                        "DB_HOST":"localhost",
                        "DB_PORT":3306,
                        "DB_DATABASE":"nestjs_news",
                        "DB_USERNAME":"root",
                        "DB_PASSWORD":"",
                        "WS_APP_URL=":"http://localhost:9898",
                        "WS_APP_PORT":9898
                },
                "env_development": {
                        "CONFIG": "dev.conf.json",
                        "NODE_ENV": "development"
                },
                "env_production" : {
                        "CONFIG": "conf.json",
                        "NODE_ENV": "production"
                },
                "exec_mode": "fork", // or cluster if that's what you want
                "name"        : "NestJS News App",
                "script"      : "/var/www/html/nestjs-ng-news/nest-server/dist/main.js", //srcipt's path
                "watch"       : false // don't restart on file changes
        }]
}
```
