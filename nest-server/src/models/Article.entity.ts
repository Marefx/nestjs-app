import { Entity, Column, PrimaryGeneratedColumn,  ManyToOne } from 'typeorm';
import SourceEntity from './Source.entity';

@Entity({name: "articles"})
export class ArticleEntity {
    @PrimaryGeneratedColumn()
    id: number;
    // https://wanago.io/2020/06/22/api-nestjs-relationships-postgres-typeorm/
    @ManyToOne(() => SourceEntity, (source: SourceEntity) => source.articles)
    source: SourceEntity;

    @Column()
    title: string;

    @Column({type: "text"})
    description: string;

    @Column({type: "text"})
    content: string;

    @Column()
    author: string;

    @Column({type: "text"})
    url: string;

    @Column({type: "text"})
    image_url: string;

    @Column()
    updated_at: Date;

    @Column()
    created_at: Date;
}

export default ArticleEntity;
