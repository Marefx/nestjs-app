import { Module } from '@nestjs/common';
import { SourcesModule } from "./sources/sources.module";
import { UsersModule } from './users/users.module';
import { ArticlesModule } from './articles/articles.module';
import { DashboardController } from './dashboard/dashboard.controller';
import { DashboardModule } from './dashboard/dashboard.module';

@Module({
    controllers: [DashboardController],
    providers: [],
    imports: [
        DashboardModule,
        UsersModule,
        ArticlesModule,
        SourcesModule,
    ]
})
export class ApiV1Module {
}
