import { Test, TestingModule } from '@nestjs/testing';
import { AccessTokensController } from "./access-tokens.controller";

describe('AccessTokensController', () => {
  let controller: AccessTokensController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [AccessTokensController],
    }).compile();

    controller = module.get<AccessTokensController>(AccessTokensController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
