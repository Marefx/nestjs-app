import {
    OnGatewayConnection,
    OnGatewayDisconnect,
    OnGatewayInit,
    SubscribeMessage,
    WebSocketGateway,
    WebSocketServer,
} from '@nestjs/websockets';
import { Logger, UseGuards } from "@nestjs/common";
import { Socket, Server } from "socket.io";
import { ArticlesWsGuard } from "./articles-ws-guard";

@WebSocketGateway(parseInt(process.env.WS_APP_PORT) || 9898)
export class ArticlesWebsocketGateway implements OnGatewayInit, OnGatewayConnection, OnGatewayDisconnect {

    @WebSocketServer() wss: Server;
    public users = 0;
    private logger: Logger = new Logger('NewsWebsocketGateway');

    afterInit(server: Server): any {
        this.logger.log(`Initialized! ${server}`);
    }

    handleConnection(client: Socket, ...args: any[]): any {
        const ip = client.handshake.headers["x-real-ip"];
        const port = client.handshake.headers["x-real-port"];
        this.users++;
        this.logger.log(`Client connected: ${ client.id } (${ip}:${port}), amount of clients connected: ${this.users}`);
        this.wss.emit('users', this.users);
    }

    handleDisconnect(client: Socket): any {
        const ip = client.handshake.headers["x-real-ip"];
        const port = client.handshake.headers["x-real-port"];
        this.users--;
        this.logger.log(`Client disconnected: ${ client.id } (${ip}:${port}), amount of clients connected: ${this.users}`);
        this.wss.emit('users', this.users);
    }

    @UseGuards(ArticlesWsGuard)
    @SubscribeMessage('messageToServer')
    handleMessage(client: Socket, payload: any): void {
        if (typeof payload === 'object') {
            this.logger.log(`(${client.id}) Received JSON message: ${JSON.stringify(payload)}`);
            return;
        }

        this.logger.log(`(${client.id}) Received message: ${payload}`);
    }
}
