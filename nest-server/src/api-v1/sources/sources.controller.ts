import { Body, Controller, Post, Request } from '@nestjs/common';
import { SourceInterface } from "../../interfaces/Source.interface";
import { SourcesService } from "./sources.service";

@Controller('api/v1/sources')
export class SourcesController {
    constructor(private readonly sourcesService: SourcesService) {}

    @Post('create')
    storeBaseArticle(@Body() source: SourceInterface, @Request() request) {
        const user = request.user;
        console.log(user);
        return this.sourcesService.create(source);
    }
}
