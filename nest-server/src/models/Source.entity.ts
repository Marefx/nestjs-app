import { Entity, Column, PrimaryGeneratedColumn,  OneToMany } from 'typeorm';
import ArticleEntity from './Article.entity'

@Entity({name: "sources"})
export class SourceEntity {
    @PrimaryGeneratedColumn()
    id: number;
    // https://wanago.io/2020/06/22/api-nestjs-relationships-postgres-typeorm/
    @OneToMany(() => ArticleEntity, (articles: ArticleEntity) => articles.source)
    articles: ArticleEntity[];

    @Column()
    name: string;

    @Column()
    url: string;

    @Column()
    description: string;

    @Column({default: false})
    enabled: boolean;
    // Will be seconds
    @Column()
    request_start: number;
    // Will be seconds
    @Column()
    request_end: number;

    @Column()
    next_request: Date;

    @Column()
    updated_at: Date;

    @Column()
    created_at: Date;
}

export default SourceEntity;
