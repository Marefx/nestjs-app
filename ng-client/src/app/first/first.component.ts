import { Component, OnDestroy, OnInit } from '@angular/core';
import { WebSocketServiceService } from '../common/websockets/web-socket-service.service';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-first',
  templateUrl: './first.component.html',
  styleUrls: ['./first.component.css']
})
export class FirstComponent implements OnInit, OnDestroy {
  private ngUnsubscribe$: Subject<boolean> = new Subject<boolean>();

  constructor(private webSocketService: WebSocketServiceService) {
  }

  ngOnInit(): void {
    this.webSocketService.listen('users')
      .subscribe((data: any) => {
        console.log(data);

        setTimeout(() => {
          this.webSocketService.emit('messageToServer', {channels: ['news', 'users']});
        }, 1000);
      });
  }

  ngOnDestroy(): void {
    this.ngUnsubscribe$.next();
    this.ngUnsubscribe$.complete();
  }
}
