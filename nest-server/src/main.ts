import { NestFactory } from '@nestjs/core';
import { NestExpressApplication } from "@nestjs/platform-express";
import { AppModule } from './app.module';
import * as fs from 'fs';

const SERVER_PORT = parseInt(process.env.EXPRESS_PORT) || 4545;

let privateKey;
let certificate;
if (process.env.APP_ENV === 'local') {
    if (process.platform === 'win32') {
        privateKey = fs.readFileSync('G://cert//localhost.key');
        certificate = fs.readFileSync('G://cert//localhost.crt');
    } else {
        privateKey = fs.readFileSync('/mnt/g//cert/localhost.key');
        certificate = fs.readFileSync('/mnt/g//cert/localhost.crt');
    }
} else if (process.env.APP_ENV === 'production') {
    privateKey = fs.readFileSync('/etc/letsencrypt/live/test.local.dev/privkey.pem');
    certificate = fs.readFileSync('/etc/letsencrypt/live/test.local.dev/fullchain.pem');
}

const options = {
    key: privateKey,
    cert: certificate,
    secure: true,
    reconnect: true,
    rejectUnauthorized: false,
    requestCert: false,
};

async function bootstrap() {
    let app = await NestFactory.create<NestExpressApplication>(AppModule);
    if (process.env.APP_ENV !== 'local') {
        app = await NestFactory.create<NestExpressApplication>(AppModule, {
            httpsOptions: options
        });
    }

    // app.useWebSocketAdapter(new WsAdapter(app));
    await app.listen(SERVER_PORT);
}

bootstrap().then(() => {
    console.log(`Server running on port ${ SERVER_PORT }!`);
});
