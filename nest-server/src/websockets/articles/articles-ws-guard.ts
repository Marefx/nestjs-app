import { CanActivate, Injectable, Logger } from "@nestjs/common";
import { Observable } from "rxjs";

@Injectable()
export class ArticlesWsGuard implements CanActivate {
    private logger: Logger = new Logger('NewsWsGuard');

    canActivate(
        context: any,
    ): boolean | any | Promise<boolean | any> | Observable<boolean | any> {
        try {
            const requestUrlParams: string = context.args[0].client.conn.request.url;
            this.logger.log(`Pass for client with URL params: ${requestUrlParams}`);
            return true;
        } catch (e) {
            console.log(e);
            return false;
        }
    }
}
