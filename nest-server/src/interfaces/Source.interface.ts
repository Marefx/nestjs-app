import ArticleEntity from "../models/Article.entity";

export interface SourceInterface {
    id?: number,
    name?: string,
    url?: string,
    description?: string,
    enabled?: boolean,
    request_start?: number,
    request_end?: number,
    next_request?: Date,
    created_at?: Date,
    updated_at?: Date,
    articles?: ArticleEntity[];
}
