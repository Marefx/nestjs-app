import { Controller, Post, Body, Get, Param } from '@nestjs/common';
import { Observable } from "rxjs";
import { AccessTokenInterface } from "../../interfaces/AccessToken.interface";
import { AccessTokensService } from "./access-tokens.service";

@Controller('api/v1/access-tokens')
export class AccessTokensController {
    constructor(private readonly accessTokensService: AccessTokensService) {}

    @Post('create')
    create(@Body() accessToken: AccessTokenInterface): Observable<AccessTokenInterface> {
        return this.accessTokensService.create(accessToken);
    }

    @Get(':id')
    findOne(@Param() params: any): Observable<AccessTokenInterface> {
        return this.accessTokensService.findOne(params.id);
    }
}
