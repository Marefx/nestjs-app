import { Module } from '@nestjs/common';
import { ArticlesWebsocketGateway } from "./articles/articles-websocket.gateway";

@Module({
    imports: [],
    exports: [],
    controllers: [],
    providers: [
        ArticlesWebsocketGateway,
    ],
})
export class WebsocketsModule {
}
