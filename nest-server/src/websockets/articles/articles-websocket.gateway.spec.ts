import { Test, TestingModule } from '@nestjs/testing';
import { ArticlesWebsocketGateway } from './articles-websocket.gateway';

describe('NewsWebsocketGateway', () => {
  let gateway: ArticlesWebsocketGateway;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ArticlesWebsocketGateway],
    }).compile();

    gateway = module.get<ArticlesWebsocketGateway>(ArticlesWebsocketGateway);
  });

  it('should be defined', () => {
    expect(gateway).toBeDefined();
  });
});
