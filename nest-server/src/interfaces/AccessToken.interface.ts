import UserEntity from "../models/User.entity";

export interface AccessTokenInterface {
    id?: number,
    token?: string,
    lats_request_ip?: string,
    lats_request_url?: string,
    last_used_at?: Date,
    created_at?: Date,
    updated_at?: Date,
    user?: UserEntity,
}
