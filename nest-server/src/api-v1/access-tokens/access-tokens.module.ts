import { Module } from '@nestjs/common';
import { TypeOrmModule } from "@nestjs/typeorm";
import AccessTokenEntity from "../../models/AccessToken.entity";
import { AccessTokensController } from "./access-tokens.controller";
import { AccessTokensService } from "./access-tokens.service";

@Module({
  controllers: [AccessTokensController],
  providers: [AccessTokensService],
  imports: [
      TypeOrmModule.forFeature([AccessTokenEntity])
  ],
  exports: [AccessTokensService],
})
export class AccessTokensModule {}
