import { Controller, Post, Body, Get, Param } from '@nestjs/common';
import { Observable } from "rxjs";
import { UserInterface } from "../../interfaces/User.interface";
import { UsersService } from "./users.service";

@Controller('api/v1/users')
export class UsersController {
    constructor(private readonly usersService: UsersService) {}

    @Post('create')
    create(@Body() user: UserInterface): Observable<UserInterface> {
        return this.usersService.create(user);
    }

    @Get(':id')
    findOne(@Param() params: any): Observable<UserInterface> {
        return this.usersService.findOne(params.id);
    }
}
