import { Module } from '@nestjs/common';
import { ConfigModule } from "@nestjs/config";
import { TypeOrmModule } from "@nestjs/typeorm";
import { ApiV1Module } from './api-v1/api-v1.module';
import { DatabaseService } from "./database/database.service";
import { WebsocketsModule } from "./websockets/websockets.module";
// import { DatabaseModule } from './database/database.module';
// import * as config from './database/ormconfig'
import config from './config';

@Module({
    imports: [
        ConfigModule.forRoot({ load: [config], isGlobal: true }),
        TypeOrmModule.forRootAsync({
            useClass: DatabaseService,
        }),
        ApiV1Module,
        WebsocketsModule,
        // DatabaseModule,
    ],
    controllers: [],
    providers: [],
})
export class AppModule {
}
