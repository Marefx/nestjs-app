import { Injectable } from '@nestjs/common';
import { InjectRepository } from "@nestjs/typeorm";
import { from, Observable } from "rxjs";
import { Repository } from "typeorm";
import { SourceInterface } from "../../interfaces/Source.interface";
import { SourceEntity } from "../../models/Source.entity";

@Injectable()
export class SourcesService {
    constructor(
        @InjectRepository(SourceEntity) private readonly sourceRepository: Repository<SourceEntity>
    ) {
    }

    create(source: SourceInterface): Observable<SourceInterface> {
        source.created_at = new Date();
        source.updated_at = new Date();
        return from(this.sourceRepository.save(source));
    }

    findOne(id: number): Observable<SourceInterface> {
        return from(this.sourceRepository.findOne(id));
    }

    findAll(): Observable<SourceInterface[]> {
        return from(this.sourceRepository.find());
    }

    deleteOne(id: number): Observable<any> {
        return from(this.sourceRepository.delete(id));
    }

    updateOne(id: number, source: SourceInterface): Observable<any> {
        source.updated_at = new Date();
        return from(this.sourceRepository.update(id, source));
    }
}
