export default () => ({
    APP_ENV: process.env.APP_ENV,
    APP_DEBUG: process.env.APP_DEBUG,
    EXPRESS_PORT: process.env.EXPRESS_PORT,
    DB_HOST: process.env.DB_HOST,
    DB_PORT: process.env.DB_PORT,
    DB_DATABASE: process.env.DB_DATABASE,
    DB_USERNAME: process.env.DB_USERNAME,
    DB_PASSWORD: process.env.DB_PASSWORD,
    WS_APP_URL: process.env.WS_APP_URL,
    WS_APP_PORT: process.env.WS_APP_PORT,
});
