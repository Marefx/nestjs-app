import { NgModule } from '@angular/core';
import { WebSocketServiceService } from './websockets/web-socket-service.service';

@NgModule({
  declarations: [],
  imports: [],
  exports: [],
  providers: [WebSocketServiceService],
})
export class GlobalCommonModule { }
