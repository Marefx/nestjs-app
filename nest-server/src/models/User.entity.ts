import { Entity, Column, PrimaryGeneratedColumn, OneToMany } from 'typeorm';
import { AccessTokenEntity } from "./AccessToken.entity";

@Entity({name: "users"})
export class UserEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @OneToMany(() => AccessTokenEntity, (accessTokens: AccessTokenEntity) => accessTokens.user)
    accessTokens: AccessTokenEntity[];

    @Column({unique: true})
    email: string;

    @Column()
    email_verified_at: Date;

    @Column()
    registration_ip: string;

    @Column({unique: true})
    username: string;

    @Column()
    password: string;

    @Column()
    firstname: string;

    @Column()
    lastname: string;

    @Column({ default: true })
    active: boolean;

    @Column()
    recovery_key: string;

    @Column()
    updated_at: Date;

    @Column()
    created_at: Date;
}

export default UserEntity;
