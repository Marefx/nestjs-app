import SourceEntity from "../models/Source.entity";

export interface ArticleInterface {
    id?: number,
    title?: string,
    description?: string,
    content?: string,
    author?: string,
    url?: string,
    image_url?: string,
    created_at?: Date,
    updated_at?: Date,
    source?: SourceEntity;
}
