import { Module } from '@nestjs/common';
import { TypeOrmModule } from "@nestjs/typeorm";
import SourceEntity from "../../models/Source.entity";
import { SourcesController } from "./sources.controller";
import { SourcesService } from "./sources.service";

@Module({
  controllers: [SourcesController],
  providers: [SourcesService],
  imports: [
    TypeOrmModule.forFeature([SourceEntity])
  ],
  exports: [SourcesService],
})
export class SourcesModule {}
