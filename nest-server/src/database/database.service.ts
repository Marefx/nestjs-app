import { Injectable } from '@nestjs/common';
import { TypeOrmModuleOptions, TypeOrmOptionsFactory } from '@nestjs/typeorm';
import { join } from "path";
import { ConnectionManager, getConnectionManager } from 'typeorm';

@Injectable()
export class DatabaseService implements TypeOrmOptionsFactory {
    async createTypeOrmOptions(): Promise<TypeOrmModuleOptions> {
        const connectionManager: ConnectionManager = getConnectionManager();
        let options: TypeOrmModuleOptions;

        if (connectionManager.has('default')) {
            options = connectionManager.get('default').options;
            await connectionManager.get('default').close();
        } else {
            options = {
                type: 'mysql',
                host: process.env.DB_HOST,
                port: parseInt(process.env.DB_PORT),
                username: process.env.DB_USERNAME,
                password: process.env.DB_PASSWORD,
                database: process.env.DB_DATABASE,
                entities: [
                    join(__dirname, '../models/*{.ts,.js}'),
                ],
                // We are using migrations, synchronize should be set to false.
                synchronize: false,
                dropSchema: false,
                // Run migrations automatically,
                // you can disable this if you prefer running migration manually.
                migrationsRun: false,
                logging: true,
                // logger: process.env.APP_ENV === 'production' ? 'file' : 'debug',
                migrations: [
                    join(__dirname, 'migrations/*{.ts,.js}')
                ],
                cli: {
                    migrationsDir: 'src/database/migrations',
                    entitiesDir: 'src/models',
                },
            };
        }
        return options;
    }
}
