export const DATABASE_CONNECTION = 'DATABASE_CONNECTION'
export const REPOSITORIES = {
    USER: 'USER_REPOSITORY',
    ROLE: 'ROLE_REPOSITORY',
    DOCUMENT_TYPE: 'DOCUMENT_TYPE'
}
