import { Injectable } from '@nestjs/common';
import { InjectRepository } from "@nestjs/typeorm";
import { from, Observable } from "rxjs";
import { Repository } from "typeorm";
import { UserInterface } from "../../interfaces/User.interface";
import { UserEntity } from "../../models/User.entity";

@Injectable()
export class UsersService {
    constructor(
        @InjectRepository(UserEntity) private readonly userRepository: Repository<UserEntity>
    ) {
    }

    create(user: UserInterface): Observable<UserInterface> {
        user.created_at = new Date();
        user.updated_at = new Date();
        return from(this.userRepository.save(user));
    }

    findOne(id: number): Observable<UserInterface> {
        return from(this.userRepository.findOne(id));
    }

    findAll(): Observable<UserInterface[]> {
        return from(this.userRepository.find());
    }

    deleteOne(id: number): Observable<any> {
        return from(this.userRepository.delete(id));
    }

    updateOne(id: number, user: UserInterface): Observable<any> {
        user.updated_at = new Date();
        return from(this.userRepository.update(id, user));
    }
}
