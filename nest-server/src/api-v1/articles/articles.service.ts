import { Injectable } from '@nestjs/common';
import { InjectRepository } from "@nestjs/typeorm";
import { from, Observable } from "rxjs";
import { Repository } from "typeorm";
import { ArticleInterface } from "../../interfaces/Article.interface";
import ArticleEntity from "../../models/Article.entity";

@Injectable()
export class ArticlesService {
    constructor(
        @InjectRepository(ArticleEntity) private readonly articleRepository: Repository<ArticleEntity>
    ) {
    }

    create(article: ArticleInterface): Observable<ArticleInterface> {
        article.created_at = new Date();
        article.updated_at = new Date();
        return from(this.articleRepository.save(article));
    }

    findOne(id: number): Observable<ArticleInterface> {
        return from(this.articleRepository.findOne(id));
    }

    findAll(): Observable<ArticleInterface[]> {
        return from(this.articleRepository.find());
    }

    deleteOne(id: number): Observable<any> {
        return from(this.articleRepository.delete(id));
    }

    updateOne(id: number, article: ArticleInterface): Observable<any> {
        article.updated_at = new Date();
        return from(this.articleRepository.update(id, article));
    }
}
