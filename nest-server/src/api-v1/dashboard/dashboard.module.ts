import { Module } from '@nestjs/common';
import { DashboardService } from './dashboard.service';
import { DashboardController } from "./dashboard.controller";

@Module({
  controllers: [DashboardController],
  providers: [DashboardService],
  imports: [],
  exports: [DashboardService],
})
export class DashboardModule {}
