import { Module } from '@nestjs/common';
import { TypeOrmModule } from "@nestjs/typeorm";
import ArticleEntity from "../../models/Article.entity";
import { ArticlesController } from './articles.controller';
import { ArticlesService } from './articles.service';

@Module({
  controllers: [ArticlesController],
  providers: [ArticlesService],
  imports: [
    TypeOrmModule.forFeature([ArticleEntity])
  ],
  exports: [ArticlesService],
})
export class ArticlesModule {}
