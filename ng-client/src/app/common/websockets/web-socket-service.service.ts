import { Injectable } from '@angular/core';
import * as io from 'socket.io-client';
import { Observable } from 'rxjs';

@Injectable()
export class WebSocketServiceService {
  socket: any;
  readonly uri: string = 'wss://test.local.dev:8020';
  readonly options: any = {
    query: 'token=123',
  };

  constructor() {
    this.socket = io.connect(this.uri, this.options);
  }

  public listen(eventName: string): any {
    return new Observable((subscriber => {
      this.socket.on(eventName, (data) => {
        subscriber.next(data);
      });
    }));
  }

  public emit(eventName: string, data: any): any {
    this.socket.emit(eventName, data);
  }
}
