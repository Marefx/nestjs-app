import { Body, Controller, Post, Request } from '@nestjs/common';
import { ArticleInterface } from "../../interfaces/Article.interface";
import { ArticlesService } from "./articles.service";

@Controller('api/v1/articles')
export class ArticlesController {
    constructor(private readonly articlesService: ArticlesService) {}

    @Post('create')
    storeBaseArticle(@Body() article: ArticleInterface, @Request() request) {
        const user = request.user;
        console.log(user);
        return this.articlesService.create(article);
    }
}
