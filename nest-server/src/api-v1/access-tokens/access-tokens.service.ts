import { Injectable } from '@nestjs/common';
import { InjectRepository } from "@nestjs/typeorm";
import { from, Observable } from "rxjs";
import { Repository } from "typeorm";
import { AccessTokenInterface } from "../../interfaces/AccessToken.interface";
import AccessTokenEntity from "../../models/AccessToken.entity";

@Injectable()
export class AccessTokensService {
    constructor(
        @InjectRepository(AccessTokenEntity) private readonly accessTokenRepository: Repository<AccessTokenEntity>
    ) {
    }

    create(accessToken: AccessTokenInterface): Observable<AccessTokenInterface> {
        accessToken.created_at = new Date();
        accessToken.updated_at = new Date();
        return from(this.accessTokenRepository.save(accessToken));
    }

    findOne(id: number): Observable<AccessTokenInterface> {
        return from(this.accessTokenRepository.findOne(id));
    }

    findAll(): Observable<AccessTokenInterface[]> {
        return from(this.accessTokenRepository.find());
    }

    deleteOne(id: number): Observable<any> {
        return from(this.accessTokenRepository.delete(id));
    }

    updateOne(id: number, accessToken: AccessTokenInterface): Observable<any> {
        accessToken.updated_at = new Date();
        return from(this.accessTokenRepository.update(id, accessToken));
    }
}
