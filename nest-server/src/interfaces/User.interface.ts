import { AccessTokenEntity } from "../models/AccessToken.entity";

export interface UserInterface {
    id?: number,
    email?: string,
    email_verified_at?: Date,
    registration_ip?: string,
    username?: string,
    password?: string,
    lastname?: string,
    firstname?: string,
    active?: boolean,
    recovery_key?: string,
    created_at?: Date,
    updated_at?: Date,
    access_tokens?: AccessTokenEntity[],
}
